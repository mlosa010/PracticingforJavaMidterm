/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeedemo;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mtsguest
 */
public class EmployeeDemo {

    /**
     * @param args the command line arguments
     */
    //Define an arrayList of Employee type.
    ArrayList<Employee> myEmployees = new ArrayList<Employee>();
    
    public static void main(String[] args)
    {
        
        Scanner myKeyboard = new Scanner(System.in);
        int hoursWorked;
        EmployeeDemo myDemo = new EmployeeDemo();
        myDemo.createEmployees();
        myDemo.computeSalary();
        
         
   }
    
    public  void createEmployees()
    {
        // Please write code below, and place all objects inside of an arrayList
        
        String lastName, firstName;
        double salary;
        SalariedEmployee salEmp;
        String[] empInfo;
        String[] hourlyempInfo;
        
        
       
        try {
            File myfile = new File("salaryEmp.txt");
            Scanner inFile = new Scanner(myfile);
            do{
            String currentLine = inFile.nextLine();
            empInfo = currentLine.split(" ");
            salary =Double.parseDouble(empInfo[2]);
            SalariedEmployee currentLineEmployee = new SalariedEmployee(empInfo[0],empInfo[1],salary);
            myEmployees.add(currentLineEmployee);
            }while(inFile.hasNextLine());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeDemo.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            System.out.println(e.getClass());
        }
        try{
        File secondFile = new File("hourlyEmp.txt");
        Scanner fileTwo =new Scanner(secondFile);
        do{
            String currentLineFileTwo = fileTwo.nextLine();
            hourlyempInfo = currentLineFileTwo.split(" ");
            salary =Double.parseDouble(hourlyempInfo[2]);
            HourlyEmployee currentHourlyEmployee =new HourlyEmployee(hourlyempInfo[0],hourlyempInfo[1],salary);
            myEmployees.add(currentHourlyEmployee);
        }while(fileTwo.hasNextLine());
        }catch(FileNotFoundException e){
            
        }catch(Exception e){
            System.out.println(e.getClass());
        }
        for(Employee o:myEmployees){
            System.out.println(o);
            
        }
        boolean fileOpened = false;
        //read the file1 that contains only SalariedEmployees
        //create SalariedEmployees subclasses, and add them to the Employee arrayList
       
        
          
        //read the file2 that contains only HourlyEmployees
        //create HourlyEmployees subclass, and add them to the Employee arrayList
    }
    
    public void computeSalary()
    {
       // loop through the arrayList, and call the polymorphic method found in the superclass
       // and all of its subclasses. Note:  the polymorphic method needs hours worked, so you will Print the results of the method's calculations   
       // need to query the user and pass that value to the polymorphic method.
        Scanner keyboard = new Scanner(System.in);
        int hours;
        double pay;
        for (Employee o: myEmployees){
            System.out.println("how many hours were worked");
            hours = keyboard.nextInt();
            System.out.println(o+"earned ");
            pay=o.weeklyPay(hours);
            System.out.println("$"+pay);
        }
       
    }
    
}
